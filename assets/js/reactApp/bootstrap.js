import React from 'react';
import ReactDOM from 'react-dom';
// import Test from './components/Test';
import VariantsTable from './components/variantsTable'
// import PriceGap from './components/priceGap';
import ProductsStateProvider from "./providers/ProductsStateProvider";

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from "@apollo/client";

export default function ReactAppBootstrap(context) {
  const client = new ApolloClient({
    uri: '/graphql',
    cache: new InMemoryCache(),
    headers: {"Authorization": `Bearer ${context.graphQLToken}`},
  });

  ReactDOM.render(
    <ApolloProvider client={client}>
      <ProductsStateProvider>
        <VariantsTable />
      </ProductsStateProvider>
    </ApolloProvider>, document.querySelector('#variants-table'));
}

