import React, {useState, useEffect, useContext} from 'react';
import up from '../../../../icons/keyboard-arrow-up.svg';
import down from '../../../../icons/keyboard-arrow-down.svg';
import {addProductsToCart} from "../../services/cartManager";
import useProductsState from "../../services/hooks/useProductsState";
import useProductsResults from "../../services/hooks/useProductsResults";

export default function DisplayVariant(props) {
  const {productId, variantsInfos, index, setVariants, variants} = props;
  const [qty, setQty] = useState(0);
  const {dispatch} = useProductsState();
  const products = useProductsResults();
  useEffect(() => {
      const item = createLineItems();
      const idx = products.findIndex(elem => elem.variantId === item.variantId);
      if (idx !== -1) {
        const newArray = [...products];
        newArray[idx] = item;
        dispatch({type: 'updateProductsToAdd', payload: newArray});
      } else {
        dispatch({type: 'updateProductsToAdd', payload: [...products, item]});
      }
  }, [qty]);

  const increment = () => {
    setQty(parseInt(qty) + 1);
  }

  const createLineItems = () => {
    return {
      productId,
      variantId: variantsInfos.entityId,
      quantity: qty,
      sku: variantsInfos.sku,
      image: variantsInfos.defaultImage.url320wide
    }
  }

  const decrement = () => {
    if (qty !== 0) {
      setQty(parseInt(qty) - 1);
    }
  }

  const changeQty = (val) => {
    setQty(val);
  }

  const addToCart = async () => {
    await addProductsToCart([createLineItems()]);
  }

  return (
    <div className="product-container__inner-container" data-product-id={productId}>
      <img src={variantsInfos.defaultImage.url320wide} alt=""/>
      <div className={`product-container__actions ${qty > 0 ? 'highlight' : ''}`}>
        <div className={`product-container__absolute-container`}>
          <div className="card-buttons">
            <a onClick={addToCart}
               className="button button--small button--primary card-figcaption-button"
            >
              Ajouter au panier
            </a>
          </div>
          <div className="product-container__quantity-box">
            <button onClick={() => decrement()}>
              <img src={down} alt=""/>
            </button>
            <input type="text" min={0} value={qty} onChange={(e) => changeQty(e.target.value)}/>
            <button onClick={() => increment()}>
              <img src={up} alt=""/>
            </button>
          </div>
        </div>
      </div>
      <div className="product-container__sku">
        {variantsInfos.sku}
      </div>
    </div>
  )

}
