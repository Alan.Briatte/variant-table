import React from 'react';
import {gql, useQuery} from "@apollo/client";

const GET_VARIANTS = gql`
query VariantsByProductId(
    $productId: Int
) {
   site {
      product(entityId: $productId) {
               name
               variants {
                  edges {
                     node {
                        sku
                        defaultImage {
                           url(width: 1000)
                        }
                        prices {
                           price {
                              ...MoneyFields
                           }
                        }
                        inventory {
                           aggregated {
                              availableToSell
                           }
                        }
                     }
                  }
               }
      }
   }
}
fragment MoneyFields on Money {
   value
   currencyCode
}
`;
export default function Test(productId) {
  console.log('ca rentre');
  const {loading, error, data} = useQuery(GET_VARIANTS, {
    variables: {"productId": productId},
  });
  console.log('ca sort');
  console.log(loading, error, data)

  if (loading) return null;
  if (error) return `Error! ${error}`;
  return data;
}
