import React from 'react';

export default function CategoryList(props) {

  const {selectContent, handleChange, level} = props;
  const trads = ['Choisissez votre marque', 'Choisissez votre modèle', 'Choisissez l\'année'];

  return (
    <div className="category-list__select">
      <select className="form-select form-select--small" defaultValue={'null'} onChange={e=> handleChange(e.target.value, selectContent, level)}>
        <option value="null" disabled>{trads[level]}</option>
        {selectContent.map((option, idx)=> (
          <option key={option.entityId} value={idx}>{option.name}</option>
        ))}
      </select>
    </div>
  )
}
