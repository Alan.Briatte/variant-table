import React, {useContext} from 'react';
import useProductsResults from "@/reactApp/services/hooks/useProductsResults";

export default function ItemList() {
  const products = useProductsResults();


  return (
    <div className={"item-list__container"}>
      <h3>Articles Selectionnés : </h3>
      {
        products.length > 0 ? products.map((pdt, idx)=> (
          pdt.quantity > 0 ?<div className={"item-list__item"} key={idx}>
            <span>{pdt.quantity}x {pdt.sku}</span>
            <img src={pdt.image} alt=""/>
          </div> : null
        )) : null
      }
    </div>
  )
}
