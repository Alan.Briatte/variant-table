import React, {useState} from 'react';
import CategoryList from '../categoryList/'

export default function SelectCategory(props) {

  const {firstLevel} = props;
  const [categoryLevel, setCategoryLevel] = useState([firstLevel[0]]);
  const [lastSelected, setLastSelected] = useState([])
  const handleChange = (idx, content, level) => {
    let newArr = [...categoryLevel];
    newArr[level + 1] = content[idx];
    setLastSelected(content[idx].path);
    setCategoryLevel(newArr);
  }

  const goToCategory = () => {
    window.location.assign(window.location.origin + lastSelected);
  }
  return (
    <>
      <div className="select-category__select-container">
        <CategoryList selectContent={firstLevel} handleChange={handleChange} level={0}/>
        {
          categoryLevel[1] ?
            <CategoryList selectContent={categoryLevel[1].children} handleChange={handleChange} level={1}/> :
            null
        }
        {
          categoryLevel[2] && categoryLevel[2].children.length ?
            <CategoryList selectContent={categoryLevel[2].children} handleChange={handleChange} level={2}/> :
            null
        }
      </div>
      <div className="select-category__action">
        <input onClick={goToCategory} className="button" type="button" value={"Rechercher"}/>
      </div>
    </>
  )
}
