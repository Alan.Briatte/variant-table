import React from 'react';
import ItemList from "@/reactApp/components/itemList";

export default function StickyAddToCart(props) {

  const {addToCart} = props;

  return (
  <div className={'sticky-add-to-cart__container'}>
    <ItemList/>
    <a
      className="button button--small button--primary card-figcaption-button"
      onClick={addToCart}
    >
      Ajouter le lot au panier
    </a>
  </div>);
}
