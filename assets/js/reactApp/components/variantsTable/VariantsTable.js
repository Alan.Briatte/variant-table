import React, {useContext, useEffect, useState} from 'react';
import useVariantsList from "../../services/hooks/useVariantsList";
import DisplayVariant from "../DisplayVariant";
import {addProductsToCart} from "../../services/cartManager";
import useProductsResults from "../../services/hooks/useProductsResults";
import StickyAddToCart from "@/reactApp/components/stickyAddToCart";

export default function VariantsTable() {
  // je dois fournir l'id à mon composant, passer en props
  const productId = parseInt($('[name="product_id"]').val());
  const {loading, error, data} = useVariantsList(productId);
  const products = useProductsResults();

  if (loading) {
    return <span>{loading.message}</span>;
  }

  if (error) {
    return <span>{error}</span>;
  }
  if (data.site.product.customFields.edges.length === 0) {
    return null;
  }

  const addToCart = async () => {
    console.log(products)
    // await addProductsToCart(products);
  }

  return (
    <>
      {/*<div className="variants-table__global-add-to-cart">*/}
      {/*  <a*/}
      {/*    className="button button--small button--primary card-figcaption-button"*/}
      {/*    onClick={addToCart}*/}
      {/*  >*/}
      {/*    Ajouter au panier*/}
      {/*  </a>*/}
      {/*</div>*/}
      <div className="variants-table__product-grid">
        {/*créer composant Pagination*/}
        {data.site.product.variants.edges.map((pdt, index) => (
          <DisplayVariant key={index}
                          index={index}
                          variantsInfos={pdt.node}
                          productId={productId}/>
        ))}
      </div>
      <div className={"sticky-add-to-cart"}>
        <StickyAddToCart addToCart={addToCart}/>
      </div>
      {/*<div className="variants-table__global-add-to-cart sticky">*/}
      {/*  <a*/}
      {/*    className="button button--small button--primary card-figcaption-button"*/}
      {/*    onClick={addToCart}*/}
      {/*  >*/}
      {/*    Ajouter au panier*/}
      {/*  </a>*/}
      {/*</div>*/}
    </>
  )
}
