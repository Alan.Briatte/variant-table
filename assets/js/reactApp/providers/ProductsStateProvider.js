import React, { useReducer } from "react";
import ProductsContext from "../context/ProductsContext";

const initialState = {
  products: [],
};



function reducer(state, action) {
  switch (action.type) {
    case "updateProductsToAdd":
      return { ...state, products: action.payload };
    default:
      throw new Error();
  }
}


export default function ProductsStateProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ProductsContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {props.children}
    </ProductsContext.Provider>
  );
}
