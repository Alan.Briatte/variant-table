import utils from "@bigcommerce/stencil-utils";
import modalFactory from "../../theme/global/modal";
import 'slick-carousel';


export async function addProductsToCart(items) {
    const cart = await getCart();

    if (cart.length > 0) {
      await addToExistingCart(items, cart)
    } else {
      await createNewCart(items)
    }
}

function getQtyInCart(html) {
  const content = document.createElement('div');
  content.innerHTML = html;
  return $('[data-cart-quantity]', content).attr('data-cart-quantity');
}

async function createNewCart(items) {
  const formattedItems = getFormattedItems(items);
  const response = await fetch("/api/storefront/carts?include=lineItems.physicalItems.options", {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "credentials": "same-origin",
    "body": JSON.stringify({lineItems: formattedItems})
  })
  const data = await response.json();

  if (!response.ok) {
    return Promise.reject("There was an issue adding items to your cart. Please try again.")
  } else {
    await refreshContent(data);
  }
}

async function addToExistingCart(items, cart) {
  const formattedItems = getFormattedItems(items);

  const response = await fetch(`/api/storefront/carts/${cart[0].id}/items`, {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "credentials": "same-origin",
    "body": JSON.stringify({lineItems: formattedItems})
  });
  const data = await response.json();
  if (!response.ok) {
    return Promise.reject("There was an issue adding items to your cart. Please try again.")
  } else {
    await refreshContent(cart)
  }
}

function getFormattedItems(items) {
  const toArray = Object.values(items);
  return toArray.filter(item => item.quantity > 0);
}

async function getCart() {

  const response = await fetch('/api/storefront/carts?include=lineItems.physicalItems.options', {
    "method": "GET",
    "headers": {
      "Content-Type": "application/json"
    },
    "credentials": "same-origin"
  });

  return await response.json();
}

async function refreshContent(data) {
  await getCart();
  openModal(data);
}

function openModal(data) {
  const previewModal = modalFactory('#previewModal')[0];

  previewModal.open();
  updateCartContent(previewModal, data.id)

}

function updateCartContent(modal, cartItemId) {
  getCartContent(cartItemId, (err, response) => {
    if (err) {
      console.log(err)
    }
    const quantity = getQtyInCart(response);
    $('body').trigger('cart-quantity-update', quantity);
    modal.updateContent(response);
    $('.previewCart-carousel').slick({
      "dots": true,
      "mobileFirst": true,
      "slidesToShow": 8,
      "slidesToScroll": 6,
      "autoplay": true,
    })
  });
}

function getCartContent(cartItemId, onComplete) {
  const options = {
    template: 'cart/bulkOrder',
    params: {
      suggest: cartItemId,
    },
  };

  utils.api.cart.getContent(options, onComplete);
}
