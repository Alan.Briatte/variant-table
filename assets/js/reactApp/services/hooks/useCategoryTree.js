import {gql, useQuery} from "@apollo/client";

const BRAND_CAT_TREE = gql
  `query getCategory($catId: Int) {
    site {
      categoryTree(rootEntityId: $catId) {
        name
        entityId
        children {
        ...CategoryFields
          children{
          ...CategoryFields
            children{
            ...CategoryFields
            }
          }
        }
      }
    }
  }
  fragment CategoryFields on CategoryTreeItem {
    name
    path
    entityId
  }`;


export default function useCategoryTree (catId) {
  return useQuery(BRAND_CAT_TREE, {variables: {
    catId
    }})
}
