import useProductsState from "./useProductsState";

const getProductsResults = state => {
  return state.products;
};

export default function useSearchResults() {
  const {state} = useProductsState();
  return getProductsResults(state);
}
