import {useContext} from "react";
import ProductsContext from "../../context/ProductsContext";

export default function useProductsState() {
  const ctx = useContext(ProductsContext);
  return ctx;
}
