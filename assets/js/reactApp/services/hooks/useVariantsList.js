import {gql, useQuery} from "@apollo/client";

const VARIANTS_LIST = gql
  `query productById($productId: Int) {
   site {
    product(entityId: $productId) {
      id
      entityId
      name
      customFields(names: ["__variants-display"]) {
        edges {
          node {
            value
            name
          }
        }
      }
      variants (first: 50) {
        edges{
          node{
            id
            sku
            options {
              edges {
                node {
                  entityId
                  displayName
                  values {
                    edges {
                      node {
                        entityId
                        label
                      }
                    }
                  }
                }
              }
            }
            entityId
            defaultImage {
             ...ImageFields
            }
            isPurchasable
          }
        }
      }
    }
  }
}
 
 fragment ImageFields on Image {
  url320wide: url(width: 320)
  url640wide: url(width: 640)
  url960wide: url(width: 960)
  url1280wide: url(width: 1280)
}
`;


export default function useVariantsList(productId) {
  return useQuery(VARIANTS_LIST, {
      variables:
        {productId}
    }
  )
}
