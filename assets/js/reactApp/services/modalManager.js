import modalFactory from "../../theme/global/modal";
// import template from "../../../../templates/components/cart/modals/bonjour";

export function openModal () {
  const previewModal = modalFactory('#previewModal')[0];
  previewModal.open();

  previewModal.updateContent(template);

}
