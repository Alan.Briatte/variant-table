export const getVariantAttributes = (variant, productId) => {
  let attributes = {};
  variant.options.edges.forEach((option) => {
    option.node.values.edges.forEach((value) => {
      attributes[option.node.entityId] = value.node.entityId;
    });
  });

  return attributes;
}
